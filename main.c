#include "snake_g.h"
#include "work_menu.h"

#define _POSIX_C_SOURCE 200112L
#define TILE_SIZE 20
#define BORDER_SIZE 4

typedef enum { Easy, Medium, Hard } GameSpeed;

/**
 * struct which is using to control game
 * @param quit flag of exit requiest
 * @param play flag of play requiest
 * @param gamespeed game speed that is using in usleep() to update map
*/
typedef struct {
	bool quit;
	bool play;
	int gamespeed;
} game_t;

/**
 * just print snake on screen
 * @param snake paramater which have coordinates of snake body
*/
void print_snake(Snake *snake){
	tile_t *temp_tile = snake->head; // head of snake
	while(temp_tile != NULL){ // while its not end
		for(int i = 0; i < TILE_SIZE; i++){ // for all pixels 
			for(int j = 0; j < TILE_SIZE; j++)
                lcd_display[temp_tile->x_coord*TILE_SIZE + i + BORDER_SIZE + TILE_SIZE*2]
                [temp_tile->y_coord*TILE_SIZE + j + BORDER_SIZE*2] = snake->colour; // print
		}
		temp_tile = temp_tile->next;
	}
}

/**
 * sent apple to display buff
 * @param board parametr that have all information about game state
*/
void print_apple(Board* board){
    for(int i = 0; i < TILE_SIZE; i++){ // for all pixels 
        for(int j = 0; j < TILE_SIZE; j++)
            lcd_display[board->appleX*TILE_SIZE + i + BORDER_SIZE + TILE_SIZE*2]
            [board->appleY*TILE_SIZE + j + BORDER_SIZE*2] = 0xFFE0; // print
    }
}

/**
 * sent screen to display buff
 * @param board parametr that have all information about game state
*/
void print_game_board(Board* board){
    for(int i = 0; i < 320; i++){
        for(int j = 0; j < 480; j++){
            if(i < TILE_SIZE + BORDER_SIZE*3)
                lcd_display[i][j]= 0;
            if(i > TILE_SIZE + BORDER_SIZE*2 && i < TILE_SIZE + BORDER_SIZE*4)
                lcd_display[i][j] = 0xFFFF;
            if(board->borders)
                if(j < BORDER_SIZE*2 || j > 480 - BORDER_SIZE*2 || i > 320 - BORDER_SIZE*2)
                    lcd_display[i][j] = 0xFFFF;
        }
    }
}

/**
 * sent scales to display buff
*/
void print_scales(){
    uint16_t colour1 = 0x0000, colour2 = 0x7BEF;
    bool other = true;
    for(int x = 0; x < 320; x += TILE_SIZE) {
        other = !other;
        for(int y = 0; y < 480; y+= TILE_SIZE){
            if(other){
                other = false;
                for(int i = 0; i < TILE_SIZE; i++)
                    for(int j = 0; j < TILE_SIZE; j++)
                        lcd_display[(x + i+ BORDER_SIZE + TILE_SIZE*2) % 320][(y + j + BORDER_SIZE*2) % 480] = colour2;
            }
            else{
                other = true;
                for(int i = 0; i < TILE_SIZE; i++)
                    for(int j = 0; j < TILE_SIZE; j++)
                        lcd_display[(x + i + BORDER_SIZE + TILE_SIZE*2) % 320][(y + j + BORDER_SIZE*2) % 480] = colour1;
            }
        }
    }
}

/**
 * main function
*/
int main(int argc, char *argv[]){

    printf("Program has started\n");
    unsigned char *mem_base; // adress leds
    unsigned char *parlcd_mem_base;// adress lcd
    srand(time(NULL));
    mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if (mem_base == NULL)
        exit(1);
    unsigned char *led1 = mem_base + 0x010; // первый фонарик
    unsigned char *led2 = mem_base + 0x014; // второй фонарик
    struct timespec loop_delay = {.tv_sec = 0};
    
    clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
    parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    if (parlcd_mem_base == NULL)
        exit(1);
    parlcd_hx8357_init(parlcd_mem_base);

    mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);

    StepInfo stepInfo = {0};
    Board board = createBoard(13, 23, OnlyPlayer, true); // gameboard for control game events
    game_t game = {.quit = false, .play = false, .gamespeed = 500000}; // defaut game parameters
	Snake snakeA = createSnake(0, 0, RIGHT, led1, 0xF81F); // first snake
	Snake snakeB = createSnake(7, 11, LEFT, led2, 0x07FF); // optional snake for AI
    Direction playerInput; // for player input

    system ("/bin/stty raw"); // set terminal
    system("stty -g > ~/.stty-save");
    system("stty -icanon min 0 time 0");

    while(true)
    { // while for menu
        start_menu: // go to menu after every game
        sent_led(snakeA.led, 0x000000ff); // sent green lights to big leds
        sent_led(snakeB.led, 0x000000ff);
        //*(volatile uint32_t *)(mem_base + SPILED_REG_LED_LINE_o) = 1073741823;
        sent_leds(mem_base, 1073741823);
        print_menu(); // print main menu on screen
        print_lcd(parlcd_mem_base); // update screen
        char c; // symbol from input
        if(read(STDIN_FILENO, &c, 1) == 1){ // if reading input is ok
            if(c == '1'){ // run new game
                game.play = true;
            }
            else if(c == '2'){ // if for choosing game type
                while(true){
                    print_gamemode(); // print gamemode menu on screen
                    //*(volatile uint32_t *)(mem_base + SPILED_REG_LED_LINE_o) = 1073741823;
                    sent_leds(mem_base, 1073741823);
                    print_lcd(parlcd_mem_base); // update screen
                    char b; // symbol from input
                    if(read(STDIN_FILENO, &b, 1) == 1){ // if reading input is ok
                        if(b == '1'){ // choose game type Only Player
                            board.gameType = OnlyPlayer;
                            break;
                        }
                        else if(b == '2'){ // choose game type AI vs AI
                            board.gameType =  AIvsAI;
                            break;
                        }
                        else if(b == '3'){ // choose game type AI vs Player
                            board.gameType = AIvsPlayer;
                            break;
                        } 
                        else if(b == '4'){ // go back
                            break;
                        }       
                    }
                } // end of if for choosing game type
            } // ending of if for choosing game speed
            else if(c == '3'){ // if for settings
                while(true){
                    print_settings(); // print settings menu on screen
                    //*(volatile uint32_t *)(mem_base + SPILED_REG_LED_LINE_o) = 1073741823;
                    sent_leds(mem_base, 1073741823);
                    print_lcd(parlcd_mem_base); // update screen
                    char d; // symbol from input
                    if(read(STDIN_FILENO, &d, 1) == 1){ // if reading input is ok
                        if(d == '1'){ // if for choosing game speed
                            while(true){
                                print_levels();
                                print_lcd(parlcd_mem_base); // update screen
                                char e; // symbol from input
                                if(read(STDIN_FILENO, &e, 1) == 1){ // if reading input is ok
                                    if(e == '1'){
                                        game.gamespeed = 500000; // easy game speed
                                        break;
                                    }
                                    else if(e == '2'){
                                        game.gamespeed = 250000; // medium game speed
                                        break;
                                    }
                                    else if(e == '3'){
                                        game.gamespeed = 100000; // high game speed
                                        break;
                                    }
                                    else if(e == '4'){ // go back 
                                        break;
                                    }
                                }
                            }
                        } // end of if for choosing game speed
                        else if(d == '2'){ // if for choosing colour of snake
                            while(true){
                                print_colours(); // print on screen colour menu
                                print_lcd(parlcd_mem_base); // update screen
                                char g; // symbol from input
                                if(read(STDIN_FILENO, &g, 1) == 1){ // if reading input is ok
                                    if(g == '1'){ 
                                        snakeA.colour = 0x07E0; // green
                                        break;
                                    }
                                    else if(g == '2'){
                                        snakeA.colour = 0xFD20; // orange
                                        break;
                                    }
                                    else if(g == '3'){
                                        snakeA.colour = 0xFFE0; // yellow
                                        break;
                                    }
                                    else if(g == '4'){ // go back
                                        break;
                                    }
                                } // end of if for choosing snake colour
                            }
                        }
                        else if(d == '3'){ // if for changing borders
                            while(true){
                                print_borders();
                                print_lcd(parlcd_mem_base); // update screen
                                char f; // symbol from input
                                if(read(STDIN_FILENO, &f, 1) == 1){ // if reading input is ok
                                    if(f == '1'){ // without borders
                                        board.borders = false;
                                        break;
                                    }
                                    else if(f == '2'){ // with borders
                                        board.borders = true;
                                        break;
                                    }
                                    else if (f == '3'){
                                        break;
                                    }
                                }
                            }
                        } // end of if for changing borders
                        else if (d == '4'){
                            break;
                        }
                    }
                }   
            } // end of if for settings
            else if(c == '4'){
                game.quit = true;
                break;
            }
        }
        if(game.play || game.quit){
            break;
        }
    }// конец while для меню
    while(true){ // while for game
        if(game.quit){ // optional break
            break;
        }

        sent_led(snakeA.led, 0x0000ff00); // default green colour on big leds
        sent_led(snakeB.led, 0x0000ff00);

        playerInput = NONE;
        char t; // symbol from input
        if(read(STDIN_FILENO, &t, 1) == 1){ // if reading input is ok
            if (t == 'w' || t  == 72){
                playerInput = UP;
            }
            else if (t == 'a' || t  == 80){
                playerInput = LEFT;
            }
            else if (t == 's' || t  == 75){
                playerInput = DOWN;
            }
            else if (t == 'd' || t  == 77){
                playerInput = RIGHT;
            }   
            else if (t == 'q'){  // optional break
                break;
            }            
        }

        print_scales();
        print_game_board(&board);
        print_apple(&board);
    
        // function that do game step, control eating apple and collisions 
        step(&board, &snakeA, &snakeB, playerInput, &stepInfo);

        if (stepInfo.winner != 0) { // Step was terminal.
            if (board.gameType != OnlyPlayer){
                if(stepInfo.winner == 1){
                    print_winner(snakeA.length, 'A');
                    print_lcd(parlcd_mem_base); // update screen
                }
                if(stepInfo.winner == 2){
                    print_winner(snakeB.length, 'B');
                    print_lcd(parlcd_mem_base); // update screen
                }
            }
            else if(board.gameType == OnlyPlayer){
                print_winner_single(snakeA.length);
                print_lcd(parlcd_mem_base); // update screen
            }
            reset(&board, &snakeA, &snakeB); // reset board and snakes for new game
            sleep(2);
            stepInfo.winner = 0;
            game.play = false;
            game.quit = false;
            goto start_menu;  // go to main menu in case game over
        }

        // print game score on board
        if(board.gameType == OnlyPlayer) // if its single player
            print_scores_single(snakeA.colour, snakeA.length);
        else // other way put optional snake on board
            print_scores(snakeA.colour, snakeB.colour, snakeA.length, snakeB.length);

        if(snakeA.appleEaten){ // new colour on left led because of apple eaten event
            sent_led(snakeA.led, 0x00FF0000);
            if(board.gameType == OnlyPlayer)
                sent_leds(mem_base, 2147483647);
                //*(volatile uint32_t *)(mem_base + SPILED_REG_LED_LINE_o) = 2147483647;
            else
                sent_leds(mem_base, 1073709056);
                //*(volatile uint32_t *)(mem_base + SPILED_REG_LED_LINE_o) = 1073709056;
            snakeA.appleEaten = false;
        }
        if(snakeB.appleEaten){ // new colour on right led because of apple eaten event
            sent_led(snakeB.led, 0x00FF0000);
            sent_leds(mem_base, 32767);
            //*(volatile uint32_t *)(mem_base + SPILED_REG_LED_LINE_o) = 32767;
            snakeB.appleEaten = false;
        }

        print_snake(&snakeA); // print optional snake on lcd if its not single game
        if(board.gameType != OnlyPlayer)
            print_snake(&snakeB);
        print_lcd(parlcd_mem_base); // update screen
        usleep(game.gamespeed);
        *(volatile uint32_t *)(mem_base + SPILED_REG_LED_LINE_o) = 0;
    } // end of while for gaming

    gameOver();
    print_lcd(parlcd_mem_base); // update screen
    //destroyBoard(&board); // clean memory 
    printf("Exit\n");
    system("stty $(cat ~/.stty-save)");
    return 0;
}
