# Semestral project for APO (Summer 2021)  

## Architektura aplikace

### Snake logic

![snake](snake.png)

  

### Snake-hardware communication

![snake-to-hardware](snake-to-hardware.png)

### Hardware communication

![hardware](hardware.png)
  

## Dohledatelnost

### Snake logic
| Name | Function |
|--|--|
| Direction | enum used to keep snake direction and to provide input |
| GameType | enum used to specify game type |
| tile_t | structure that holds (x, y) coordinates of a snake's tile and a pointer to the next one |
| Snake | structure that holds all the necessary information about a snake |
| Board| structure that holds 2d array for collision checks, bounds, game type, and apple coordinates|
| StepInfo | structure that is returned each game update with immediate information about the game state|
| createBoard | function that returns Board instance after initializing 2D array and placing an apple for the new board|
| getComputerInput | function that returns Direction enum to be used as an AI input |
| createSnake | function that creates Snake instance with specified parameters |
| destroyBoard | function that frees memory of the given board|
| placeApple| function that places an apple on the specified board 
| destroySnakeTile | function that recursively frees snake's memory 
| reset | function that resets the board |
| step | preprocessor function for the simulationStep that switches between different game types |
| moveSnake | function that handles actual game logic, responsible for updating a single snake based on some input |
| simulationStep | function that is responsible for advancing the game board state by one step |

### Hardware communication
| Name | Function |
|--|--|
| print_char | function to print needed char |
| print_text | function to printing text |
| sent_led | function to send colour to big led diod |
| sent_leds | function to send colour to small led diods |
| print_lcd | function to send data from own buffer display to real 
| black_display | ust set mono colorized screen for update |
| print_menu | function to write main menu od display |
| print_winner | function to write score winner od display |
| gameOver | function to write exit message after program ending od display |
| print_snake | print snake on screen |
| print_apple | sent apple to display buff |
| print_game_board | sent screen to display buff |
| print_scales | sent scales to display buff |
| game_t | struct which is using to control game |


## Popis kompilace, instalace, spuštění

### Start
You can run the project on your own MZ_APO board, as well as using the board provided by the university. To start the project, you will need an MZ_APO board and a personal PC with linux subsystem. To connect to the board through the terminal and monitor the status of the boards live use the link https://cw.felk.cvut.cz/courses/apo/ at https://cw.fel.cvut.cz/wiki/courses/b35apo/documentation/mz_apo-howto/start.

### Required software
1. ARM cross compiler toolchain
 ```bash
    sudo apt-get update
    sudo apt install crossbuild-essential-armhf
    sudo apt-get install libc6-armel-cross libc6-dev-armel-cross binutils-arm-linux-gnueabi libncurses5-dev
```
 
 2. Access rights for the MicroZed APO in CTU

If the certificate is not valid, run the following commands:
 ```bash
scp username@postel.felk.cvut.cz:/opt/zynq/ssh-connect/mzapo-root-key
chmod go-rwx mzapo-root-key  
ssh-add mzapo-root-key  
ssh -o 'ProxyJump=username@postel.felk.cvut.cz' root@ip
```
You can choose one of these IP of boards on CTU in Prague:
``` bash
192.168.202.203
192.168.202.212
192.168.202.207
192.168.202.127
192.168.202.211
192.168.202.204
192.168.202.213
```
 Use this link to get more information: https://cw.fel.cvut.cz/wiki/courses/b35apo/en/documentation/mz_apo-howto/start
  
### Program launch after we connected:
#### First approach: clone the repository
``` bash
cd apo/binrep/ # Go to the desired folder
mkdir project # Create a new folder
cd project # Go to the newly created folder
git clone git@gitlab.fel.cvut.cz:oryshiva/apo-snake.git
```

#### Second approach: create a new folder and files
 ```bash
mkdir NewFolder # new folder
nano NewFile # new file
``` 

#### After either of the above steps you can run the program
``` bash
make # Compile file using Makefile
./snake # Run the executable
```

## Manuál

### Navigating the menu
Once you're connected and the game is running, use keys '1'-'4' to navigate the menu. To go one level up the menu use '4'. When you select "New gmae" the menu will close and the game will start with the specified game mode, which is set to "Only Player" by default. There are 3 game modes in total: "Only Player" , "AI vs AI", "AI vs Player". You can choose them in the "Game Modes" submenu.

#### Menu hierarchy
 1. New game
 2. Game modes
     1. Single
     2. AI vs AI
     3. AI vs Human
	 4. Return
 3. Options
	 1. Level
		 1. Easy
		 2. Medium
		 3. Hard
		 4. Return
	 2. Colour
 		 1. Green
		 2. Orange
		 3. Yellow
		 4. Return
	 3. Borders
 		 1. No borders
		 2. With borders
		 3. Return
 4. Quit

### Playing the game
To control the snake use keys WASD. The snake will respond to input only if its direction is changed. Both snakes start at length 1 and grow by 1 cell when they eat apples. The AI player tries to avoid collisions by doing basic lookahead, which makes for a pretty convincing AI. It does not, however, predicts the future location of the other snake. The game will go on until either the player or the AI hits the border or snake's body. At the end the winner's score is displayed and you can go back to the menu. The program can be terminated at any point by pressing 'q'.

### Perififeries used
- Standard input via SSH is used to get navigate the menu and control the snake
- The relevant RGB diode is lit each time a snake eats an apple
- Active LED lights correspond to the length of each snake

## Equipment

### Description of the hardware of the used MicroZed processor board MICROZED EVALUATION KIT 
 1. ADSAES-Z7MB-7Z010-G  Xylinx Zynq 7Z010
 2. Base Chip: Xilinx Zynq-7000 All Programmable SoC
 3. Type: Z-7010, part XC7Z010
 4. CPU: Dual ARM Cortex ™ -A9 MPCore ™ @ 866 (NEON ™ & Single/DoublePrecision Floating Point)
 5. 2x L1 32 kB data + 32 kB instruction, L2 512 KB
 6. FPGA: 28K Logic Cells (~ 430K ASIC logic gates, 35 kbit)
 7. Computing units in FPGAs: 100 GMACs
 8. FPGA memory: 240 KB
 9. Memory on MicroZed board: 1GB
 10. Operating system: GNU/Linux
 11. GNU LIBC (libc6) 2.19-18 + deb8u7
 12. Linux kernel 4.9.9-rt6-00002-ge6c7d1c
 13. Distribution: Debian Jessie
 
 ### Interfaces accessible directly on MicroZed board
 1. 1G ETHERNET 
 2. USB Host
 3. A connector serial port UART1 via converter to USB
 4. USB micro-B
 5. micro SD card on the board is Flash
 6. one user LED
 7. user button and reset button
 
  **More information at http://microzed.org/product/microzed**
 

## Made by Kirill Shibanov and Ivan Oryshchenko
