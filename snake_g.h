#ifndef SNAKE_G
#define SNAKE_G
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

typedef enum { OnlyPlayer, AIvsPlayer, AIvsAI } GameType;
typedef enum { NONE=0, UP=1, RIGHT=2, DOWN=-1, LEFT=-2 } Direction;

typedef struct{
    int x_coord;
    int y_coord;
    void *next;
}tile_t;

typedef struct {
	tile_t *head;
	Direction dir;
	int length;
	bool appleEaten;
	unsigned char *led;
	uint16_t colour;
} Snake;

typedef struct {
	int** board;
	int sizeX;
	int sizeY;
	GameType gameType;
	bool borders;
	int appleX;
	int appleY;
} Board;

typedef struct {
	int winner;
} StepInfo;

Board createBoard(int boardSizeX, int boardSizeY, GameType gameType, bool borders) {
	// Initialize 2d board array.
	int* values = calloc(boardSizeX * boardSizeY, sizeof(int));
    int** rows = malloc(boardSizeX * sizeof(int*));
    for (int i = 0; i < boardSizeX; ++i)
    {
        rows[i] = values + i * boardSizeY;
    }

	Board board = {rows, boardSizeX, boardSizeY, gameType, borders, 0, 0};
	placeApple(&board);

    return board;
}

Direction getComputerInput(Board* board, Snake* snake) {
	Direction input;
	// printf("Snake head->at: %d, %d\n", snake->head->x_coord, snake->head->y_coord);
	int dy = board->appleX - snake->head->x_coord;
	int dx = board->appleY - snake->head->y_coord;
	// printf("dx: %d, dy: %d\n", dx, dy);

	Direction xDir = dx > 0 ? RIGHT : LEFT;
	Direction yDir = dy > 0 ? DOWN : UP;

	bool priority = false;
	if (dx == 0 || xDir == -(snake->dir)) {
		input = yDir;
		priority = true;
	}

	if (dy == 0 || yDir == -(snake->dir)) {
		input = xDir;
		priority = true;
	}

	if (!priority) {
		input = (rand() & 1) ? xDir : yDir;
	}

	// Prevent potential collision.
	if (input == UP && ((snake->head->x_coord - 1) < 0 || board->board[snake->head->x_coord - 1][snake->head->y_coord] == 1)) {
		input = xDir;
	} else if (input == DOWN && ((snake->head->x_coord + 1) >= board->sizeX || board->board[snake->head->x_coord + 1][snake->head->y_coord] == 1)) {
		input = xDir;
	} else if (input == LEFT && ((snake->head->y_coord - 1) < 0 || board->board[snake->head->x_coord][snake->head->y_coord-1] == 1)) {
		input = yDir;
	} else if (input == RIGHT && ((snake->head->y_coord + 1) >= board->sizeY || board->board[snake->head->x_coord][snake->head->y_coord+1] == 1)) {
		input = yDir;
	}

	return input;
}

Snake createSnake(int x, int y, Direction dir, unsigned char *led, uint16_t colour) {
	tile_t *head = malloc(sizeof(tile_t));
	head->x_coord = x;
	head->y_coord = y;
	head->next = NULL;
	Snake snakeA = {head, dir, 1, false, led, colour};
	return snakeA;
}

void destroyBoard(Board* board)
{
    free(*(board->board));
    free(board->board);
}

void placeApple(Board* board) {
	do {
		board->appleX = rand() % board->sizeX;
		board->appleY = rand() % board->sizeY;
	} while (board->board[board->appleX][board->appleY] == 1);
	board->board[board->appleX][board->appleY] = 2;
}

void destroySnakeTile(tile_t* tile) {
	if (tile == NULL) {
		return;
	} else {
		destroySnakeTile(tile->next);
		free(tile);
	}
}

void reset(Board* board, Snake* snakeA, Snake* snakeB) {
	for(int i = 0; i < board->sizeX; i++) {
		for (int j = 0; j < board->sizeY; j++) {
			board->board[i][j] = 0;
		}
	}

	destroySnakeTile(snakeA->head->next);
	tile_t *headA = malloc(sizeof(tile_t));
	headA->x_coord = 4;
	headA->y_coord = 4;
	headA->next = NULL;
	snakeA->head = headA;
	snakeA->length = 1;

	destroySnakeTile(snakeB->head->next);
	tile_t *headB = malloc(sizeof(tile_t));
	headB->x_coord = 4;
	headB->y_coord = 8;
	headB->next = NULL;
	snakeB->head = headB;
	snakeB->length = 1;

	placeApple(board);
}

void step(Board* board, Snake* snakeA, Snake* snakeB, Direction playerInput, StepInfo* stepInfo) {
	switch (board->gameType) {
		case OnlyPlayer:
			simulationStep(board, snakeA, playerInput, NULL, NONE, stepInfo);
			break;
		case AIvsPlayer:
			simulationStep(board, snakeA, playerInput, snakeB, getComputerInput(board, snakeB), stepInfo);
			break;
		case AIvsAI:
			simulationStep(board, snakeA, getComputerInput(board, snakeA), snakeB, getComputerInput(board, snakeB), stepInfo);
			break;
	}
}

bool moveSnake(Board* board, Snake* snake, Direction input) {
	// Change direction.
	if (input != 0 && snake->dir != input && -(snake->dir) != input) {
		snake->dir = input;
	}

	tile_t *head = malloc(sizeof(tile_t));
	head->x_coord = snake->head->x_coord;
	head->y_coord = snake->head->y_coord;
	head->next = snake->head;
	snake->head = head;

	// Move head->in direction.
	switch(snake->dir) {
		case(UP):
			snake->head->x_coord -= 1;
			break;
		case(RIGHT):
			snake->head->y_coord += 1;
			break;
		case(DOWN):
			snake->head->x_coord += 1;
			break;
		case(LEFT):
			snake->head->y_coord -= 1;
			break;
	}

	// Check against board edges.
	if (board->borders) {
		if (snake->head->x_coord < 0 || snake->head->x_coord >= board->sizeX || snake->head->y_coord < 0 || snake->head->y_coord >= board->sizeY) {
			return true;
		}
	} else {
		if (snake->head->x_coord < 0) {
			snake->head->x_coord = board->sizeX - 1;
		}
		if(snake->head->x_coord >= board->sizeX) {
			snake->head->x_coord = 0;
		}
		if (snake->head->y_coord < 0) {
			snake->head->y_coord = board->sizeY - 1;
		}
		if(snake->head->y_coord >= board->sizeY) {
			snake->head->y_coord = 0;
		}
	}

	// Check against other snakes and apples.
	bool snakeSnakeCollision = false;
	snake->appleEaten = false;
	switch(board->board[snake->head->x_coord][snake->head->y_coord]) {
		case(0): // Empty.
			break;
		case(1): // Snake body.
			snakeSnakeCollision = true;
			break;
		case(2): // Apple.
			snake->length += 1;
			snake->appleEaten = true;
			break;
	}

	// Update board.
	board->board[snake->head->x_coord][snake->head->y_coord] = 1;

	// Update tail.
	int count = 0;
	tile_t* curr = snake->head;
	while (curr != NULL) {
		if (snake->length == count) {
			free(curr->next);
			board->board[curr->x_coord][curr->y_coord] = 0;
			curr->next = NULL;
			break;
		}
		curr = curr->next;
		count++;
	}

	return snakeSnakeCollision;
}

void simulationStep(Board* board, Snake* snakeA, Direction inputA, Snake* snakeB, Direction inputB, StepInfo* stepInfo) {
	bool terminal = false;
	bool appleEaten = false;
	terminal = moveSnake(board, snakeA, inputA);
	if (terminal) {
		stepInfo->winner = 2;
	}
	appleEaten |= snakeA->appleEaten;

	if (board->gameType != OnlyPlayer) {
		terminal = moveSnake(board, snakeB, inputB);
		if (terminal) {
			stepInfo->winner = 1;
		}
		appleEaten |= snakeB->appleEaten;
	}

	if (appleEaten) {
		placeApple(board);
	}
}


#endif
