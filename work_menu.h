#ifndef WORK_MENU
#define WORK_MENU
#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"

uint16_t lcd_display[320][480]; // buffer of lcd screen
font_descriptor_t *font = &font_winFreeSystem14x16; // 

/**
 * function to print needed char
 * @param x x starting coordinate on display to write chunk
 * @param y y starting coordinate on display to write chunk
 * @param needed_char is a char we want to print
 * @param square length of chunk
 * @param colour colour of char
 */
void print_char(int x, int y, char ch, unsigned colour, int square) {
    uint16_t *ptr = font->bits + (ch - font->firstchar) *font->height;
    ch -= font->firstchar;
    for(int i = 0; i < font->height; i++){
        uint16_t buffer = *(ptr++);   
        for(int j = 0; j < font->maxwidth; j++){
            if ((buffer & 0x8000) != 0){                
                for(int m = 0; m < square; m++) // for all pixels in square    
                    for(int n = 0 ; n < square; n++)
                        lcd_display[y + i*square + m][x + j*square + n] = colour;
            }    
            buffer <<= 1;
        }
    }
}

/**
 * function to printing text
 * @param x x starting coordinate on display to write text
 * @param y y starting coordinate on display to write text
 * @param length length of the text
 * @param ch pointer to printing text
 * @param square length of chunk
 * @param colour colour of text
 * @param space_between is a space between two chars
*/
void print_text(int x, int y, int length, int square, char *ch, uint16_t colour, int space_between){
    for (int i = 0; i < length; i++){ // for all symbols in line
        print_char(x, y, *ch, colour, square); //print symbol on lcd buffer
        char c = *ch++;
        c -= font->firstchar;
        x += font->width[c]*square + space_between; // moving on x because its next symbol
	}
}

/**
 * function to send colour to led diod
 * @param led adress in memory to write needed colour
 * @param coulour needed colour
*/
void sent_led(unsigned char *led, uint32_t colour){
	*(volatile uint32_t*)(led) =  colour; // set colour	
}

/**
 * function to turn on led diods
 * @param led adress in memory to write needed colour
 * @param coulour needed colour
*/
void sent_leds(unsigned char *mem_base, int lamps){
	*(volatile uint32_t *)(mem_base + SPILED_REG_LED_LINE_o) = lamps;	
}

/**
 * function to send data from own buffer display to real
 * @param parlcd_mem_base adress in memory of lcd display
*/
void print_lcd(unsigned char *led){
    for (int i = 0; i < 320 ; i++) // for all pixels
        for (int j = 0; j < 480 ; j++)
            parlcd_write_data(led, lcd_display[i][j]); // sending
    parlcd_write_cmd(led, 0x2c);
}

/**
 * just set mono colorized screen for update
*/
void black_display(){
    for (int i = 0; i < 320 ; i++)
        for (int j = 0; j < 480 ; j++)
            lcd_display[i][j] = 0x000F;
}

/**
 * function to write main menu od display
*/
void print_menu(){
    char main_menu1[] = "SLITHER", main_menu2[] = "New game";
    char main_menu3[] = "Game modes", main_menu4[] = "Options", main_menu5[] = "Quit";
    black_display();
    print_text(50, 15, 8, 4, &main_menu1, 0xffff, 10);
    print_text(50, 20, 8, 4, &main_menu1, 0xf800, 10);
    print_text(20, 100, 9, 3, &main_menu2, 0x07FF, 5);
    print_text(20, 145, 11, 3, &main_menu3, 0x07FF, 5);
    print_text(20, 190, 8, 3, &main_menu4, 0x07FF, 5);
    print_text(20, 235, 5, 3, &main_menu5, 0x07FF,  5);
}

/**
 * function to write settings menu od display
*/
void print_settings(){
    char settings1[] = "Options", settings2[] = "Level", settings3[] = "Colour";
    char settings4[] = "Borders", settings5[] = "Return";
    black_display();
    print_text(50, 20, 8, 4, &settings1,  0xAFE5, 10);
    print_text(20, 100, 6, 3, &settings2, 0x07FF, 5);
    print_text(20, 145, 6, 3, &settings3,  0x07FF, 5);
    print_text(20, 190, 8, 3, &settings4,  0x07FF, 5);
    print_text(20 , 235, 7, 3, &settings5,  0x07FF, 5);
}

/**
 * function to write level changing menu od display
*/
void print_levels(){
    char level1[] = "Level", level2[] = "Easy", level3[] = "Medium";
    char level4[] = "Hard", level5[] = "Return";
    black_display();
    print_text(50, 20,  6, 4, &level1, 0xAFE5, 10);
    print_text(20, 100, 5, 3, &level2, 0x07E0, 5);
    print_text(20, 145, 6, 3, &level3, 0xFFE0, 5);
    print_text(20, 190, 4, 3, &level4, 0xF800, 5);
    print_text(20, 235, 7, 3, &level5, 0x07FF, 5);
}

/**
 * function to write colour changing menu od display
*/
void print_colours(){
    char colour1[] = "Colour", colour2[] = "Green";
    char colour3[] = "Orange", colour4[] = "Yellow", colour5[] = "Return";
    black_display();
    print_text(50, 20, 7, 4, &colour1, 0xAFE5, 10);
    print_text(20, 100, 6, 3, &colour2, 0x07E0, 5);
    print_text(20, 145, 7, 3, &colour3, 0xF800, 5);
    print_text(20, 190, 7, 3, &colour4, 0xFFE0, 5);
    print_text(20, 235, 7, 3, &colour5, 0x07FF, 5);
}

/**
 * function to write border options menu od display
*/
void print_borders(){
    char borders1[] = "Borders", borders2[] = "No borders";
    char borders3[] = "With borders", borders4[] = "Return";
    black_display();
    print_text(50, 20, 8, 4, &borders1,  0xAFE5, 10);
    print_text(20, 100, 11, 3, &borders2, 0x07FF, 5);
    print_text(20, 145, 13, 3, &borders3, 0x07FF, 5);
    print_text(20, 190, 7, 3, &borders4, 0x07FF, 5);
}

/**
 * function to write game mode menu od display
*/
void print_gamemode(){
    char gamemode1[] = "Gamemode", gamemode2[] = "Single";
    char gamemode3[] = "AI vs AI", gamemode4[] = "AI vs Human", gamemode5[] = "Return";
    black_display();
    print_text(50, 20, 9, 4, &gamemode1, 0xAFE5, 5);
    print_text(20, 100, 7, 3, &gamemode2, 0x07FF, 5);
    print_text(20, 145, 9, 3, &gamemode3, 0x07FF, 5);
    print_text(20, 190, 12, 3, &gamemode4, 0x07FF, 5);
    print_text(20, 235, 7, 3, &gamemode5, 0x07FF, 5);
}

/**
 * function to write score winner od display
*/
void print_winner(int score, char player){
    char Winner1[] = "Winner:  ", Winner2[] = "Score:   ";
    black_display();
    sprintf(&Winner1[7],"%c", player);
    print_text(60, 70, 9, 5, &Winner1, 0x07FF, 5);
    sprintf(&Winner2[7], "%d", score);
    print_text(130, 140, 10, 3, &Winner2, 0xFFE0, 10);
}

/**
 * function to write single score winner od display
*/
void print_winner_single(int score){
    char Winner1[] = "Game over!", Winner2[] = "Score:   ";
    black_display();
    print_text(20, 70, 11, 5, &Winner1, 0x07FF, 5);
    sprintf(&Winner2[7], "%d", score);
    print_text(130, 140, 10, 3, &Winner2, 0xFFE0, 10);
}

/**
 * function to write scores while game od display
*/
void print_scores(uint16_t colour1, uint16_t colour2, int score1, int score2){
    char scoreA[] = "A:  ", scoreB[] = "B:  ";
	sprintf(&scoreA[3], "%d", score1); // give score to text 
    sprintf(&scoreB[3], "%d", score2);
	print_text(30, 1, 5, 2, &scoreA, colour1, 10);
	print_text(350, 1, 5, 2, &scoreB, colour2, 10);
}

/**
 * function to write score in single game od display
*/
void print_scores_single(uint16_t colour, int score1){
    char scoreA[] = "A:  ";
	sprintf(&scoreA[3], "%d", score1);  // give score to text 
	print_text(30, 1, 5, 2, &scoreA, colour, 10);
}

/**
 * function to write exit message after program ending od display
*/
void gameOver(){
    char gameOver_text[] = "Have a nice day!", poweredBy1_text[] = "Powered by:";
    char poweredBy2_text[] = "shibakir", poweredBy3_text[] = "oryshiva";
    for (int i = 0; i < 320 ; i++)
        for (int j = 0; j < 480 ; j++)
            lcd_display[i][j] = 0;
    print_text(14, 145, 17, 4, &gameOver_text, 0xffff, 0);
    print_text(17, 145, 17, 4, &gameOver_text, 0xf800, 0);
    print_text(3, 3, 12, 1, &poweredBy1_text, 0xf800, 0);
    print_text(3, 19, 9, 1, &poweredBy2_text, 0xAFE5, 0);
    print_text(3, 29, 9, 1, &poweredBy3_text, 0xAFE5, 0);
}

#endif
